package com.thoughtworks;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.elasticsearch.spark.sql.api.java.JavaEsSparkSQL;

public class ImportApplication {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("should be one argument");
            return;
        }
        String inputFilePath = args[0];

        ApplicationPropertiesReader propertiesReader = ApplicationPropertiesReader.getInstance();
        String esNodes = propertiesReader.read("es.nodes");
        if (null == esNodes) {
            System.err.println("no property es.nodes");
        }
        String esPort = propertiesReader.read("es.port");
        if (null == esNodes) {
            System.err.println("no property es.port");
        }
        String index = propertiesReader.read("userprofile.index");
        if (null == esNodes) {
            System.err.println("no property userprofile.index");
        }
        String type = propertiesReader.read("userprofile.type");
        if (null == esNodes) {
            System.err.println("no property userprofile.type");
        }

        SparkSession spark = SparkSession.builder()
                .appName("csv2Es")
                .config("es.nodes", esNodes)
                .config("es.port", esPort)
                .getOrCreate();
        Dataset<Row> userProfiles = spark.read().format("csv").option("header", "true").load(inputFilePath);
        userProfiles.show();

        String resource = index + "/" + type;
        JavaEsSparkSQL.saveToEs(userProfiles, resource);
    }

}
