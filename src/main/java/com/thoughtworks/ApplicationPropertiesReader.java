package com.thoughtworks;

import com.thoughtworks.utils.MapUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Map;

public class ApplicationPropertiesReader {

    private static ApplicationPropertiesReader instance = new ApplicationPropertiesReader();

    private Map<String, Object> propertiesMap;

    private ApplicationPropertiesReader() {
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("application.yml");
        propertiesMap = yaml.load(inputStream);
    }

    public static ApplicationPropertiesReader getInstance() {
        return instance;
    }

    public String read(String path) {
        return MapUtils.get(propertiesMap, path)
                .map(String::valueOf)
                .orElse(null);
    }

}
